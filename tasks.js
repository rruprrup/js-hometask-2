/**
 * 1. Напиши функцию createCounter, которая возвращает функцию, которая будет считать количество вызовов.
 * Нужно использовать замыкание.
 */


function createCounter() {
  let counter = 0;
  function countCall() {
    counter++;
    return counter;
  };
  return countCall;
};

/**
 * 2. Не меняя уже написаный код (можно только дописывать новый),
 * сделай так, чтобы в calculateHoursOnMoon
 * переменная HOURS_IN_DAY была равна 29,5, а в функции calculateHoursOnEarth
 * эта переменная была 24.
 */

let HOURS_IN_DAY = 24;

function calculateHoursOnMoon (days) {
  const HOURS_IN_DAY = 29.5;
    return days * HOURS_IN_DAY;
}

function calculateHoursOnEarth (days) {
  return days * HOURS_IN_DAY;
}

/**
 * 3. Допиши функцию crashMeteorite, после которой
 * продолжительность дня на земле (из предыдущей задачи)
 * изменится на 22 часа
 */

function crashMeteorite (days) {
  HOURS_IN_DAY = 22;
}

/**
 * 4. Функция createMultiplier должна возвращать функцию, которая
 * при первом вызове возвращает произведение аргумента
 * функции createMultiplier и переменной a, при втором — аргумента и b,
 * при третьем — аргумента и c, с четвертого вызова снова a, потом b и так далее.
 *
 * Например:
 * const func = createMultiplier(2);
 * func(); // 16 (2*a)
 * func(); // 20 (2*b)
 * func(); // 512 (2*c)
 * func(); // 16 (2*a)
 *
 */

function createMultiplier (num) {
  let counter = 0;
  const a = 8;
  const b = 10;
  const c = 256;
  function func () {
    counter++;
    if (counter % 3 === 0) {
      return num * c;
    } else if (counter % 3 === 2) {
      return num * b;
    } else {
      return num * a;
    }
  }
  return func;
}

/**
 * 5. Напиши функцию createStorage, которая будет уметь хранить
 * какие-то данные и манипулировать ими.
 * Функция должна возвращать объект с методами:
 * - add — метод, который принимает на вход любое количество аргументов и добавляет их в хранилище;
 * - get — метод, возвращающий хранилище;
 * - clear — метод, очищающий хранилище;
 * - remove — метод, который принимает на вход элемент, который нужно удалить из хранилища и удаляет его;
 *
 * Примеры использования смотри в тестах.
 */

function createStorage () {
  let arr = [];
  return {
      add: function () {
        for (let i = 0; i < arguments.length; i++) {
          arr.push(arguments[i]);
        }
      },
      get: function () {
        return arr;
      },
      clear: function () {
        arr = [];
      },
      remove: function (forRemove) {
        let index = arr.indexOf(forRemove);
        if (index !== -1) {
          arr.splice(index, 1);
        }          
      }
  }
}

/**
 * 6*. Реализовать через let функцию поочередного
 * добавления в массив чисел от 0 до 10 с интервалом в 500ms.
 *
 * Для выполнения задачи должен быть цикл от 0 до 10,
 * внутри которого должен быть setTimeout(func, 50) с функцией внутри,
 * которая должна наполнить массив result числами от 0 до 10
*/

function letTimeout () {
    const result = [];
  
    for(let i = 0; i <= 10; i++) {

      window.setTimeout(function () {
        result.push(i)
      }, 50);      
    }
    return result; // числа от 0 до 10
}

/**
 * 7*. Реализовать такую же функцию, как letTimeout, только через var.
 * В комментарии объяснить, почему и как она работает
 */
/*
Значения i поднимаются из цикла в функцию varTimeout и через 50мс она берёт значение i не все числа от 0 до 10
(как приобъявлении с let в прошлом задании), а только 11 — тк это последнее значение i нужное для остановки цикла, 
и 11 раз повторяет 11, потому что именно это мы просим функцию сделать — добавить в массив 11 раз число i.
Для решения этой задачи надо вместо var написать let (из разряда очевидного, да?). А ещё переназначить i внутри 
функции setTimeout, чтобы с var переменная не поднималась снова наверх
*/
function varTimeout () {
  const result = [];

  for(var i = 0; i <= 10; i++) {
     let j = i;
     window.setTimeout(function () {
      result.push(j)
    }, 50);      
  }
  return result; // числа от 0 до 10
}

module.exports = {
    calculateHours: {
        onEarth: calculateHoursOnEarth,
        onMoon: calculateHoursOnMoon,
    },
    crashMeteorite,
    createMultiplier,
    createStorage,
    createCounter,
    letTimeout,
    varTimeout
};
